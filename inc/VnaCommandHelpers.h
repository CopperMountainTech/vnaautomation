#ifndef VNCOMMANHELPERS_H
#define VNCOMMANHELPERS_H

#include "VnaAutomationGlobal.h"
#include "AutomationException.h"
#include <VnaAnalyzer.h>


namespace Planar {
namespace VNA {
namespace Automation {


VNAAUTOMATION_EXPORT unsigned int getChannelIndex(unsigned int channelIndex);


}//Automation
}//VNA
}//Planar

#endif // VNCOMMANHELPERS_H
