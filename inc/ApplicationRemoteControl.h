#pragma once

#include <QObject>

#include <QtGui/QRgb>

#include "AutomationGlobal.h"

namespace Automation {

//подробности Obzor_304_804_Programming_Manual_SCPI.pdf

enum class ChannelSchemeType
{
    X1,     X2R2C1,  X2R1C2,  X3R3C1,
    X3R1C3, X3R2C2,  X4R4C1,  X4R2C2,
    X6R3C2, X6R2C3,  X8R4C2,  X8R2C4,
    X9,     X12R4C3, X12R3C4, X16
};

enum class ColorSchemeType : bool { Norm, Inverted };

enum class MarkerTextAlignmentType { None, Vertical, Horizontal };

enum class MarkerDisplayType { AllCharts, ActiveChart };

class AUTOMATION_EXPORT ApplicationRemoteControl : public QObject {
    Q_OBJECT
public:
    ApplicationRemoteControl () = default;
    ~ApplicationRemoteControl () = default;

signals:
    //! Считать цвет фона графиков измерений
    void GetPlotBackgroundColor(QRgb );
    //! Установить цвет фона графиков измерений
    void SetPlotBackgroundColor(QRgb);

    //! считывает цвет линий сетки и оцифровки графиков измерений
    void GetPlotGridColor(QRgb);
    //! Установить цвет линий сетки и оцифровки графиков измерений
    void SetPlotGridColor(QRgb);

    //! Установить в начальное состояние настройки дисплея
    void ResetDisplayConfiguration();

    //! Включить/отключить обновления дисплея
    void EnableDisplayUpdate(bool enable);
    //! Считать состояние ВКЛ/ОТКЛ обновления дисплея
    bool DisplayUpdateEnabled();


    //Разрешить/запретить индикацию знака
    //Брак во время допускового контроля или теста пульсаций
    void EnableFaultIndicator(bool enable);

    //! Cчитать инверсию цвета графиков измерений
    void GetDisplayColorScheme(ColorSchemeType);
    //! Установить инверсию цвета графиков измерений
    void SetDisplayColorScheme(ColorSchemeType);

    /*!
        Прячет графическое содержимое главного окна программы.
        Окно программы очищается и выводится надпись "Remote Control"
    */
    void HideDisplay();

    /*!
        Восстанавливает графическое содержимое главного окна
        программы, спрятанное командой DISP:HIDE
    */
    void ShowDisplay();

        //Устанавливает или считывает состояние Вкл./Откл. увеличения окна активного канала
    bool ActiveChannelMaximized();
    void MaximizeActiveChannel(bool maximize);

        //Устанавливает или считывает состояние Вкл./Откл. увеличения
        //активного графика указанного канала
    bool ActiveChartMaximized();
    void MaximizeActiveChart(bool maximize);

    //! Cчитывать номер схемы расположения окон каналов
    void GetDisplayChannelScheme(ChannelSchemeType);
    //! Установить номер схемы расположения окон каналов
    void SetDisplayChannelScheme(ChannelSchemeType);

    /*!
        Когда обновление дисплея отключено (команда DISP:ENAB
        установлена ОТКЛ), выполняет однократное обновление дисплея
    */
    void ImmediateUpdateDisplay();

    /*!
        Cчитать тип выравнивания на экране данных маркеров различных графиков,
        когда признак индикации маркеров только для активного графика отключен
    */
    void GetMarkerTextAlignment(MarkerTextAlignmentType);
    /*!
        Установить тип выравнивания на экране данных маркеров различных графиков,
        когда признак индикации маркеров только для активного графика отключен
    */
    void SetMarkerTextAlignment(MarkerTextAlignmentType);

    //! Включить/отключить индикацию маркеров только для активного графика
    void SetMarkerDisplayMode(MarkerDisplayType mode);

    //! Разрешить индикацию заголовка канала
    void EnableChannelTitle(bool enable);

    /*!
     * \brief Переводит измеритель в локальный режим работы, при котором
            все кнопки передней панели, мышь и сенсорный экран функционируют
     */
    void EnableLocalManagment ();

    /*!
     * \brief Переводит измеритель в удаленный режим работы, при котором
              все кнопки передней панели, мышь и сенсорный экран заблокированы
     */
    void DisableLocalManagment ();

    /*!
     * \brief Переводит измеритель в удаленный режим работы, при котором
              все кнопки передней панели, мышь и сенсорный экран заблокированы
              кроме кнопки ReturnToLocal
     */
    void DisableLocalManagmentWithReturn ();
};
}
