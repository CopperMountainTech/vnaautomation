#pragma once

#include <stdexcept>

namespace Automation { namespace SCPI {
class MarkerIndexOutOfRange : public std::out_of_range {
public:
    explicit MarkerIndexOutOfRange (std::string const & msg) : std::out_of_range(msg) {

    }
};
}
}
