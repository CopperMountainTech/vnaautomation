#ifndef VNACOMMANDS_H
#define VNACOMMANDS_H

#include "VnaAutomationGlobal.h"


namespace Planar {
namespace VNA {
namespace Automation {


VNAAUTOMATION_EXPORT void registerVnaCommands();


}//Automation
}//VNA
}//Planar

#endif // VNACOMMANDS_H
