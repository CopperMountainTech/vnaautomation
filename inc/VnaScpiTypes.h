#ifndef VNASCPIT_H
#define VNASCPIT_H

#include "cmds.h"
#include "scpi.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Numeric Value Param Spec Definitions   */
#define NAU ((enum enUnits *)0) /* No Alternate Units */
#define ALT_UNITS_LIST const enum enUnits /* List of Alternate Units */
#define NUM_TYPE const struct strSpecAttrNumericVal /* Numeric Val Attribs */

/**************************************************************************************/
/* Alternative Units     */
/* -----------------     */
/* USER: Create a list for each set of Alternative Units supported (if any) */
/* Notes:      */
/* a) Always include U_END as last member of each list  */
/**************************************************************************************/
static ALT_UNITS_LIST  eAltDegCAndF[] = {U_CELSIUS, U_FAHREN, U_END};      /* Deg C & Deg F  */
static ALT_UNITS_LIST  eAltPowWAndDB[] = {U_WATT, U_DBM, U_DB_W, U_END};   /* Pow: W, dBm, dBW */
/* Stimulus Units */
static ALT_UNITS_LIST  eAltStimulus[] = {U_HERTZ, U_SEC, U_WATT, U_DBM, U_DB_W, U_END};
/* Response Units */
static ALT_UNITS_LIST  eAltResponse[] = {U_DB, U_DEG, U_SEC, U_UNIT, U_END};

/**************************************************************************************/
/* Numerical Value Types     */
/* ---------------------     */
/* USER: Create a structure for each type of Numerical Value supported */
/* Notes:      */
/* a) See JPA-SCPI Parser User Manual for details   */
/**************************************************************************************/
/*  Default Alternative Exponent of  */
/* Name Units Units Default Units  */
/* ----- ------- ----------- -------------  */
static NUM_TYPE sNoUnits  = {U_NONE, NAU, 0};  /* No Units */
static NUM_TYPE sVolts    = {U_VOLT, NAU, 0};  /* Volts only */
static NUM_TYPE sAmps     = {U_AMP,  NAU, 0};  /* Amps only */
static NUM_TYPE sOhms     = {U_OHM,  NAU, 0};  /* Ohms only */
static NUM_TYPE sWatts    = {U_WATT, NAU, 0};  /* Watts only */
static NUM_TYPE sDBWatts  = {U_DB_W, NAU, 0};  /* Decibel Watts only */
static NUM_TYPE sDecibell = {U_DB,   NAU, 0};  /* Decibel only */
static NUM_TYPE sJoules   = {U_JOULE, NAU, 0}; /* Joules only */
static NUM_TYPE sFarads   = {U_FARAD, NAU, 0}; /* Farads only */
static NUM_TYPE sHenrys   = {U_HENRY, NAU, 0}; /* Henrys only */
static NUM_TYPE sHertz    = {U_HERTZ, NAU, 0}; /* Hertz only */
static NUM_TYPE sSecs     = {U_SEC,   NAU, 0}; /* Seconds only */
static NUM_TYPE sKelvin   = {U_KELVIN, NAU, 0}; /* Deg Kelvin only */
static NUM_TYPE sCelsius  = {U_CELSIUS,NAU, 0}; /* Deg Celsius only */
static NUM_TYPE sFahren   = {U_FAHREN, NAU, 0}; /* Deg Fahrenheit only*/
static NUM_TYPE sTemperature= {U_KELVIN, eAltDegCAndF, 0}; /* Kelvin; also allow C & F*/
static NUM_TYPE sPower    = {U_DBM,  eAltPowWAndDB, 0}; /* Power: dBm, W, dBW */
static NUM_TYPE sStimulus = {U_NONE, eAltStimulus, 0}; /* Stimulus Units */
static NUM_TYPE sResponse = {U_NONE, eAltResponse, 0}; /* Response Units */
static NUM_TYPE sDegree   = {U_DEG,  NAU, 0};      /* Degree only*/

/**************************************************************************************/
/* Character Data Sequences    */
/* ------------------------    */
/* USER: Create an entry for each Character Data Sequence supported.  */
/* Notes:      */
/* a) Separate each Item in a Sequence with a pipe (|) char  */
/* b) Enter required characters in Uppercase, optional characters in Lowercase */
/* c) Quotes (single and double) are allowed but must be matched  */
/* d) Do not include spaces within the strings   */
/**************************************************************************************/
/*  Name   Sequence   */
/*  ----   ---------------   */
static CHDAT_SEQ SeqMinMax[]  = "MINimum|MAXimum";
static CHDAT_SEQ SeqMinMaxDef[]  = "MINimum|MAXimum|DEFault";
static CHDAT_SEQ SeqTriggerSource[] = "INTernal|EXTernal|BUS";
static CHDAT_SEQ SeqParameter[] = "S11|S21|A|B|Pow|UNKnown|R";
static CHDAT_SEQ SeqFormat[] = "MLOGarithmic|PHASe|UPHase|GDELay|SWR|REAL|IMAGinary|MLINear|SLINear|SLOGarithmic|SCOMplex|SMITh|SADMittance|PLINear|PLOGarithmic|POLar";
static CHDAT_SEQ SeqSweepType[] = "LINear|LOGarithmic|UNKnown|CAL|SEGMent|POWer";
static CHDAT_SEQ SeqPosNegBoth[] = "POSitive|NEGative|BOTH";
static CHDAT_SEQ SeqSearchType[] = "MAXimum|MINimum|NPEAK|PEAK|LPEak|RPEak|TARGet|LTARget|RTARget";
static CHDAT_SEQ SeqMathFunc[] = "DIVide|MULTiply|SUBTract|ADD|NORMal";
static CHDAT_SEQ SeqStoreType[] = "STATe|CSTate|DSTate|CDSTate";
static CHDAT_SEQ SeqDataFormat[] = "ASCii|REAL|REAL32";
static CHDAT_SEQ SeqByteOrder[] = "NORMal|SWAPped";
static CHDAT_SEQ SeqMarkerSet[] = "STARt|STOP|CENTer|RLEVel|DELay";
static CHDAT_SEQ SeqRlimDispVal[] = "OFF|ABSolute|MARgin";
static CHDAT_SEQ SeqCoefficient[] = "ER|ED|ES|ET|EX|EL";
static CHDAT_SEQ SeqStandard[] = "OPEN|SHORt|LOAD|THRU|NONE";
static CHDAT_SEQ SeqConvFunc[] = "IMPedance|ADMittance|INVersion|CONJugation";
static CHDAT_SEQ SeqGatShape[] = "MINimum|NORMal|WIDE|MAXimum";
static CHDAT_SEQ SeqGatType[] = "BPASs|NOTCh";
static CHDAT_SEQ SeqFunction[] = "PTPeak|STDEV|MEAN|MAXimum|MINimum|PEAK|APEak|ATARget";
static CHDAT_SEQ SeqBWSearchType[] = "BPASs|NOTCh";
static CHDAT_SEQ SeqBWSearchRef[] = "MAXimum|MARKer";
static CHDAT_SEQ SeqTimeStim[] = "IMPulse|STEP";
static CHDAT_SEQ SeqTimeType[] = "BPASs|LPASs";
static CHDAT_SEQ SeqSegmBase[] = "LINear|OBASe";
static CHDAT_SEQ SeqImage[] = "NORMal|INVert";
static CHDAT_SEQ SeqPaint[] = "COLor|GRAY|BW";
static CHDAT_SEQ SeqAlign[] = "NONE|VERTical|HORizontal";
static CHDAT_SEQ SeqRegister[] = "A|B|C|D";
static CHDAT_SEQ SeqSnPFormat[] = "RI|DB|MA";
static CHDAT_SEQ SeqRefSource[] = "INTernal|EXTernal";
static CHDAT_SEQ SeqUserChar[] = "CHAR0|CHAR1|CHAR2|CHAR3|CHAR4|CHAR5";

/**************************************************************************************/
/* Character Data Types    */
/* --------------------    */
/* USER: Create a structure for each type of Character Data Sequence supported */
/*       Optional: Remove structures not required   */
/* Notes:      */
/* a) See JPA-SCPI Parser User Manual for details   */
/**************************************************************************************/
/*    Default Alternative */
/* Name  Sequence Item # Parameter  */
/* ----  -------- ------- ----------- */
static CHDAT_TYPE  sMinMaxNoUnits  = { SeqMinMax,       NO_DEF,   P_NUM, (void *)&sNoUnits};  /* MIN|MAX|<value>        */
static CHDAT_TYPE  sMinMaxVolts  = { SeqMinMax,       NO_DEF,   P_NUM, (void *)&sVolts};   /* MIN|MAX|<volts>        */
static CHDAT_TYPE sMinMaxDefVolts  = { SeqMinMaxDef, NO_DEF, P_NUM, (void *)&sVolts}; /* MIN|MAX|DEF|<volts> */
static CHDAT_TYPE  sMinMaxAmps  = { SeqMinMax,       NO_DEF,   P_NUM, (void *)&sAmps};   /* MIN|MAX|<amps>         */
static CHDAT_TYPE sMinMaxDefAmps  = { SeqMinMaxDef, NO_DEF, P_NUM, (void *)&sAmps}; /* MIN|MAX|DEF|<amps> */
static CHDAT_TYPE  sMinMaxOhms  = { SeqMinMax,       NO_DEF,   P_NUM, (void *)&sOhms};   /* MIN|MAX|<ohms>         */
static CHDAT_TYPE sMinMaxDefOhms  = { SeqMinMaxDef, NO_DEF, P_NUM, (void *)&sOhms}; /* MIN|MAX|DEF|<ohms> */
static CHDAT_TYPE  sMinMaxHertz  = { SeqMinMax,       NO_DEF,   P_NUM, (void *)&sHertz};   /* MIN|MAX|<hertz>        */
static CHDAT_TYPE sMinMaxDefHertz  = { SeqMinMaxDef, NO_DEF, P_NUM, (void *)&sHertz}; /* MIN|MAX|DEF|<hertz> */
static CHDAT_TYPE  sMinMaxSecs  = { SeqMinMax,       NO_DEF,   P_NUM, (void *)&sSecs};   /* MIN|MAX|<seconds>      */
static CHDAT_TYPE sMinMaxDefSecs  = { SeqMinMaxDef, NO_DEF, P_NUM, (void *)&sSecs}; /* MIN|MAX|DEF|<seconds> */
static CHDAT_TYPE  sMinMaxPower  = { SeqMinMax,       NO_DEF,   P_NUM, (void *)&sPower};   /* MIN|MAX|<power>        */
static CHDAT_TYPE  sMinMaxStimulus = { SeqMinMax,       NO_DEF,   P_NUM, (void *)&sStimulus}; /* MIN|MAX|<stimulus>     */
static CHDAT_TYPE  sMinMaxResponse = { SeqMinMax,       NO_DEF,   P_NUM, (void *)&sResponse}; /* MIN|MAX|<response>     */
static CHDAT_TYPE sTriggerSource = { SeqTriggerSource, NO_DEF, ALT_NONE };  /* INT|EXT|MAN|BUS */
static CHDAT_TYPE sParameter = { SeqParameter, NO_DEF, ALT_NONE };  /* S11|S21|A|B|R */
static CHDAT_TYPE sFormat = { SeqFormat, NO_DEF, ALT_NONE };  /* MLOG|PHAS|GDEL... */
static CHDAT_TYPE sPosNegBoth = { SeqPosNegBoth, NO_DEF, ALT_NONE };  /* POS|NEG|BOTH */
static CHDAT_TYPE sSearchType = { SeqSearchType, NO_DEF, ALT_NONE };  /* MAX|MIN|PEAK|LPE|RPE|TARG|LTAR|RTAR */
static CHDAT_TYPE sMathFunc = { SeqMathFunc, NO_DEF, ALT_NONE };  /* NORM|SUBT|DIV|ADD|MULT */
static CHDAT_TYPE sStoreType = { SeqStoreType, NO_DEF, ALT_NONE };  /* STAT|CST|DST|CDST */
static CHDAT_TYPE sSweepType = { SeqSweepType, NO_DEF, ALT_NONE };  /* LIN|LOG|SEGM|POW */
static CHDAT_TYPE sDataFormat = { SeqDataFormat, NO_DEF, ALT_NONE };  /* ASC|REAL|REAL32 */
static CHDAT_TYPE sByteOrder = { SeqByteOrder, NO_DEF, ALT_NONE };  /* NORM|SWAP  */
static CHDAT_TYPE sMarkerSet = { SeqMarkerSet, NO_DEF, ALT_NONE };  /* STAR|STOP|CENT|RLEV|DEL*/
static CHDAT_TYPE sRlimDispVal = { SeqRlimDispVal, NO_DEF, ALT_NONE };  /* OFF|ABS|MAR */
static CHDAT_TYPE sCoefficient = { SeqCoefficient, NO_DEF, ALT_NONE };  /* ER|ED|ES|ET|EX|EL */
static CHDAT_TYPE sStandard = { SeqStandard, NO_DEF, ALT_NONE };  /* OPEN|SHORt|LOAD|THRU|NONE */
static CHDAT_TYPE sConvFunc = { SeqConvFunc, NO_DEF, ALT_NONE };  /* IMP|ADM|INV|CONJ */
static CHDAT_TYPE sGatShape = { SeqGatShape, NO_DEF, ALT_NONE };  /* MAX|WIDE|NORM|MIN */
static CHDAT_TYPE sGatType = { SeqGatType, NO_DEF, ALT_NONE };  /* BPAS|NOTC  */
static CHDAT_TYPE sFunction = { SeqFunction, NO_DEF, ALT_NONE };  /* PTP|STDEV|MEAN|MAX|MIN|PEAK|APE|ATAR */
static CHDAT_TYPE sBWSearchType = { SeqBWSearchType, NO_DEF, ALT_NONE };  /* BPAS|NOTC */
static CHDAT_TYPE sBWSearchRef = { SeqBWSearchRef, NO_DEF, ALT_NONE };  /* MAX|MARK */
static CHDAT_TYPE sTimeStim = { SeqTimeStim, NO_DEF, ALT_NONE };  /* IMP|STEP */
static CHDAT_TYPE sTimeType = { SeqTimeType, NO_DEF, ALT_NONE };  /* BPAS|LPAS */
static CHDAT_TYPE sSegmBase = { SeqSegmBase, NO_DEF, ALT_NONE };  /* LINear|OBASe */
static CHDAT_TYPE sImage = { SeqImage, NO_DEF, ALT_NONE };  /* NORMal|INVert */
static CHDAT_TYPE sPaint = { SeqPaint, NO_DEF, ALT_NONE };  /* COLor|GRAY|BW */
static CHDAT_TYPE sAlign = { SeqAlign, NO_DEF, ALT_NONE };  /* NONE|VERTical|HORizontal */
static CHDAT_TYPE sRegister = { SeqRegister, NO_DEF, ALT_NONE };  /* A|B|C|D */
static CHDAT_TYPE sSnPFormat = { SeqSnPFormat, NO_DEF, ALT_NONE };  /* RI|DB|MA */
static CHDAT_TYPE sRefSource = { SeqRefSource, NO_DEF, ALT_NONE };  /* INTertnal|EXTernal */
static CHDAT_TYPE sUserChar  = { SeqUserChar, NO_DEF, ALT_NONE };  /* CHAR0|CHAR1|CHAR2|CHAR3|CHAR4|CHAR5 */



#ifdef __cplusplus
}
#endif

#endif // VNASCPIT_H
