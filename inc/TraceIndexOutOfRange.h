#pragma once

#include <stdexcept>

namespace Automation { namespace SCPI {
class TraceIndexOutOfRange : public std::out_of_range {
public:
    explicit TraceIndexOutOfRange (std::string const & msg) : std::out_of_range(msg) {

    }
};
}
}
